# IA168 Algorithmic Game Theory

This book follows the course IA168 Algorithmic Game Theory taught by assoc. prof. Brázdil on FI MUNI as it appeared in Fall 2023. All of this course is almost 1-to-1 based on his materials and I only typesetted it into a (in my opinion) more readable format.

*In recent years, huge amount of research has been done at the borderline between game theory and computer science, largely motivated by the emergence of the Internet. The aim of the course is to provide students with basic knowledge of fundamental game theoretic notions and results relevant to applications in computer science. The course will cover classical topics, such as general equilibrium theory and mechanism design, together with modern applications to network routing, scheduling, online auctions etc. We will mostly concentrate on computational aspects of game theory such as complexity of computing equilibria and connections with machine learning.*

## Running

This book is created in Quarto and comes with 2 targets: PDF and HTML. You can render them both in the directory of this project by running
```
quarto render --to all
```

## Notice

I typesetted this entire material based on Brázdil's presentation and as such I claim only the right to this material itself (which I distribute under the MIT license) and **not** the selection and style of presentation of the content.

## Contribution

Please, feel free to open an Issue or a Merge Request in case something seems wrong.