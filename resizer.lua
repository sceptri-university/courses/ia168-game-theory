-- Filter images with this function if the target format is LaTeX.
if FORMAT:match 'latex' then
	function Image (elem)
		-- Surround all images with image-centering raw LaTeX.
		elem.attributes.width = '50%'
		return elem
	end
end